
CREATE TABLE t_feedback
(
    feedback_id    BIGSERIAL PRIMARY KEY,
    event_id  BIGINT references t_event(event_id),
    feedback_author     VARCHAR(20),
    feedback_message      VARCHAR(100)        NOT NULL,
    event_rate          NUMERIC(1) NOT NULL,
    check (event_rate BETWEEN  1 AND 6)
);
COMMENT ON TABLE t_feedback IS 't_feedbacks table.';
COMMENT ON COLUMN t_feedback.feedback_id IS 'Primary key of t_feedbacks table.';
COMMENT ON COLUMN t_feedback.feedback_author IS 'name of the author of feedback. A not null column.';
COMMENT ON COLUMN t_feedback.feedback_message IS 'message left as feedback';
COMMENT ON COLUMN t_feedback.event_rate IS 'event rate from 1 to 5 stars';


