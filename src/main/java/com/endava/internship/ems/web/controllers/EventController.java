package com.endava.internship.ems.web.controllers;

import com.endava.internship.ems.web.dto.EventDTO;
import com.endava.internship.ems.web.dto.FeedbackDTO;
import com.endava.internship.ems.services.model.entities.Event;
import com.endava.internship.ems.services.model.entities.Feedback;
import com.endava.internship.ems.services.service.EventService;
import com.endava.internship.ems.services.service.FeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
@RequestMapping("${endpoints.events.url}")
public class EventController {

    private final EventService eventService;
    private final FeedbackService feedbackService;

    public EventController(final EventService eventService,
                           final FeedbackService feedbackService) {
        this.eventService = eventService;
        this.feedbackService = feedbackService;
    }

    @GetMapping("/displayEvents")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok().body(eventService.getAll());
    }

    @GetMapping("/findEvent")
    public ResponseEntity<?> findEventByName(@RequestParam(value = "event_name") String name) {
        return ResponseEntity.ok().body(eventService.findByEventName(name));
    }

    @PostMapping("/createEvent")
    public ResponseEntity<?> createEvent(@RequestBody EventDTO dto) {
        Event event = eventService.eventFromDTO(dto);

        Optional<Event> savedEvent = eventService.save(event);
        return savedEvent
                .map(ResponseEntity::<Object>ok)
                .orElseGet(() -> ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Registration failed!"));
    }

    @RequestMapping("/addFeedback")
    public ResponseEntity<?> addFeedback(@RequestBody FeedbackDTO dto) {
        return feedbackService.findById(dto.getId())
                .flatMap(feedbackService::save)
                .map(Feedback::dto)
                .map(ResponseEntity::<Object>ok)
                .orElse(ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Feedback creation failed!"));
    }
}