package com.endava.internship.ems.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@JsonInclude(NON_NULL)
public class EventDTO {

    // TODO: validate
    private Long id;
    private LocalDateTime dateTime;
    private String description;
    private String name;
    private Integer totalPlaces;
    private Integer placesAvailable;
    private String location;
    private List<UserDTO> participants;
    private List<FeedbackDTO> feedbackList;
}
