package com.endava.internship.ems.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@JsonInclude(NON_NULL)
public class FeedbackDTO {

    // TODO: validate
    private Long id;
    private Boolean anonymous;
    private Long userId;
    private String message;
    private Integer rate;
}
