package com.endava.internship.ems.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@JsonInclude(NON_NULL)
public class SignUpDTO {

    // TODO: validate
    @NotNull(message = "Password cannot be null.")
    @Size(min = 6,
            message = "Password must be min 6 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$")
    private String password;

    @NotNull(message = "Name cannot be null.")
    @Size(min = 3, max = 20,
            message = "Name must be between 3 and 20 characters")
    private String name;

    @Email(message = "Enter a valid mail address.")
    private String email;

    private String address;

    private String telephoneNumber;
}