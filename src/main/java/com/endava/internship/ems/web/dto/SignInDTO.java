package com.endava.internship.ems.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@JsonInclude(NON_NULL)
public class SignInDTO {

    @Email(message = "Enter a valid mail address.")
    private String email;

    @NotNull(message = "Phone number cannot be null.")
    private String telephoneNumber;

    @NotNull(message = "Password cannot be null.")
    private String password;
}
