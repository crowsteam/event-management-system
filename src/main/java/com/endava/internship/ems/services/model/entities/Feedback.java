package com.endava.internship.ems.services.model.entities;

import com.endava.internship.ems.web.dto.FeedbackDTO;
import com.endava.internship.ems.services.model.entities.annotations.Column;
import com.endava.internship.ems.services.model.entities.annotations.Id;
import com.endava.internship.ems.services.model.entities.annotations.ManyToOne;
import com.endava.internship.ems.services.model.entities.annotations.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Table("t_feedback")
public class Feedback {

    @JsonIgnore
    @Id
    @Column("feedback_id")
    private Long id;

    @Column("feedback_author")
    private User user;

    @Column("anonymous")
    private Boolean anonymous;

    @Column("feedback_message")
    private String message;

    @Column("event_rate")
    private Integer rate;

    @ManyToOne(targetEntity = Event.class)
    private Event event;

    public FeedbackDTO dto() {
        return FeedbackDTO.builder()
                .id(getId())
                .anonymous(anonymous)
                .message(getMessage())
                .rate(getRate())
                .userId(anonymous ? null : getUser().getId())
                .build();
    }
}
