package com.endava.internship.ems.services.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidResultSetException extends RuntimeException {
    public InvalidResultSetException(String message) {
        super(message);
    }
}
