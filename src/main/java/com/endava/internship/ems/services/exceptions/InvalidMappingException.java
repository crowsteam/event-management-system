package com.endava.internship.ems.services.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidMappingException extends RuntimeException {
    public InvalidMappingException(String message) {
        super(message);
    }
}
