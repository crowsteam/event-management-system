package com.endava.internship.ems.services.service;

import com.endava.internship.ems.web.dto.EventDTO;
import com.endava.internship.ems.web.dto.UserDTO;
import com.endava.internship.ems.services.model.entities.Event;
import com.endava.internship.ems.services.model.entities.User;
import com.endava.internship.ems.dao.repositories.EventRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class EventService {

    private final EventRepository eventRepository;

    private final UserService userService;

    public EventService(final EventRepository eventRepository,
                        final UserService userService) {
        this.eventRepository = eventRepository;
        this.userService = userService;
    }

    public Event eventFromDTO(EventDTO dto) {

        List<User> participants = dto.getParticipants().stream()
                .map(UserDTO::getId)
                .map(userService::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());

        return Event.builder()
                .id(dto.getId())
                .dateTime(dto.getDateTime())
                .description(dto.getDescription())
                .name(dto.getName())
                .totalPlaces(dto.getTotalPlaces())
                .placesAvailable(dto.getPlacesAvailable())
                .location(dto.getLocation())
                .participants(participants)
                .build();
    }

    public List<Event> getAll() {
        return eventRepository.findAll();
    }

    public Optional<Event> save(Event event) {
        return eventRepository.save(event);
    }

    public List<Event> findByEventName(String eventName) {
        return eventRepository.findByColumn("event_name", eventName);
    }

    public Optional<Event> findById(Long id) {
        return eventRepository.findById(id);
    }
}


