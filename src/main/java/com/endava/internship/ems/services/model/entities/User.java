package com.endava.internship.ems.services.model.entities;

import com.endava.internship.ems.services.model.entities.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Table("t_user")
public class User {

    @Id
    @Column("user_id")
    private Long id;

    @Column("user_name")
    private String name;

    @ManyToMany(targetEntity = Event.class)
    @JoinTable("t_user_event")
    private List<Event> events;

    @Column("encrypted_password")
    private String encryptedPassword;

    @Column("email")
    private String email;

    @Column("address")
    private String address;

    @Column("telephone_number")
    private String telephoneNumber;
}
