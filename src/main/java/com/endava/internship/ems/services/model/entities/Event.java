package com.endava.internship.ems.services.model.entities;

import com.endava.internship.ems.services.model.entities.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Table("t_event")
public class Event {

    @Id
    @Column("event_id")
    private Long id;

    @Column("date_time")
    private LocalDateTime dateTime;

    @Column("description")
    private String description;

    @Column("event_name")
    private String name;

    @Column("total_places")
    private Integer totalPlaces;

    @Column("available_places")
    private Integer placesAvailable;

    @ManyToMany(targetEntity = User.class)
    @JoinTable("t_user_event")
    private List<User> participants;

    @OneToMany(targetEntity = Feedback.class)
    @JoinTable("t_feedback_event")
    private List<Feedback> feedbackList;

    @Column("location")
    private String location;

    public Integer getPlacesAvailable() {
        return totalPlaces == null ? 0 :
                participants == null ? totalPlaces : totalPlaces - participants.size();
    }
}
