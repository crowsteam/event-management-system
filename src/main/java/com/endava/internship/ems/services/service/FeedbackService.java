package com.endava.internship.ems.services.service;

import com.endava.internship.ems.services.model.entities.Feedback;
import com.endava.internship.ems.dao.repositories.FeedbackRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FeedbackService {

    private final FeedbackRepository feedbackRepository;

    public FeedbackService(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    public List<Feedback> getAll() {
        return feedbackRepository.findAll();
    }

    public Optional<Feedback> save(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }

    public Optional<Feedback> findById(Long id) {
        return feedbackRepository.findById(id);
    }
}
