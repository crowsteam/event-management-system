package com.endava.internship.ems.dao.repositories;

import com.endava.internship.ems.dao.config.DatabaseConnection;
import com.endava.internship.ems.services.model.entities.Feedback;
import com.endava.internship.ems.dao.repositories.interfaces.IFeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FeedbackRepository implements IFeedbackRepository {

    private final DatabaseConnection dbConnection;

    @Autowired
    public FeedbackRepository(DatabaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public Class<Feedback> getEntityClass() {
        return Feedback.class;
    }

    @Override
    @SuppressWarnings("ReturnPrivateMutableField")
    public DatabaseConnection getDatabaseConnection() {
        return dbConnection;
    }
}
