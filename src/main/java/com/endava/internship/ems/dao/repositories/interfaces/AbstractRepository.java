package com.endava.internship.ems.dao.repositories.interfaces;

import com.endava.internship.ems.dao.config.DatabaseConnection;
import com.endava.internship.ems.services.model.entities.annotations.ManyToMany;
import com.endava.internship.ems.services.model.entities.annotations.OneToMany;
import com.endava.internship.ems.services.exceptions.InvalidMappingException;
import com.endava.internship.ems.services.exceptions.InvalidResultSetException;
import com.endava.internship.ems.dao.utils.EMSUtils;
import com.endava.internship.ems.dao.utils.EntityUtils;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.endava.internship.ems.dao.utils.EntityUtils.*;
import static java.util.Collections.emptyList;

public interface AbstractRepository {

    /**
     * Get database connection object.
     *
     * @return database connection
     */
    DatabaseConnection getDatabaseConnection();

    /**
     * Generates REMOVE sql query for an entity.
     *
     * @param entity the entity
     * @return SQL query string
     */
    default <X> String sqlRemove(final X entity,
                                 final Class<? extends X> clazz) {
        final String table = EntityUtils.getTableName(clazz);
        final String id = getIdColumnName(clazz);
        final String value = getIdColumnValue(clazz, entity).toString();
        return "delete from " + table + " where " + id + " = " + value + " returning *";
    }

    /**
     * Generates INSERT sql query for an entity.
     *
     * @param entity the entity
     * @return SQL query string
     */
    default <X> String sqlInsert(final X entity,
                                 final Class<? extends X> clazz) {
        final String table = EntityUtils.getTableName(clazz);
        final String[] columnNames = getColumnNames(clazz);
        final Object[] columnValues = getColumnValues(clazz, entity);
        return sqlInsert(table, columnNames, columnValues);
    }


    /**
     * Generates INSERT sql query for given table with given column values.
     *
     * @param table        table name
     * @param columnNames  column names
     * @param columnValues column values
     * @return SQL query string
     */
    default String sqlInsert(final String table,
                             final String[] columnNames,
                             final Object[] columnValues) {
        final StringBuilder columns = new StringBuilder();
        final StringBuilder values = new StringBuilder();
        for (int i = 0; i < columnNames.length; i++) {
            if (Optional.ofNullable(columnValues[i]).isPresent()) {
                if (columns.length() > 0) {
                    columns.append(", ");
                    values.append(", ");
                }
                String columnName = columnNames[i];
                Object columnValue = getColumnStringValue(columnValues[i]);
                columns.append(columnName);
                values.append(columnValue);
            }
        }
        return "insert into " + table + "(" + columns + ") values (" + values + ") returning *";
    }

    /**
     * Generates UPDATE sql query for an entity.
     *
     * @param entity the entity
     * @return SQL query string
     */
    default <X> String sqlUpdate(final X entity,
                                 final Class<? extends X> clazz) {
        final String table = EntityUtils.getTableName(entity);
        final String[] columnNames = getColumnNames(clazz);
        final Object[] columnValues = getColumnValues(clazz, entity);
        final StringBuilder columns = new StringBuilder();
        for (int i = 0; i < columnNames.length; i++) {
            if (columns.length() > 0) {
                columns.append(", ");
            }
            String columnName = columnNames[i];
            Object columnValue = getColumnStringValue(columnValues[i]);
            columns.append(columnName).append(" = ").append(columnValue);
        }
        return columns.length() == 0 ? ""
                : "update " + table + " set " + columns + " returning *";
    }

    /**
     * Generates SELECT sql query for a table.
     *
     * @param table the database table
     * @return SQL query string
     */
    default String sqlSelectAll(final String table) {
        return "select * from " + table;
    }

    /**
     * Generates SELECT sql query for a table.
     *
     * @param table  the database table
     * @param column the column name
     * @param value  the column value
     * @return SQL query string
     */
    default String sqlSelectAllByColumn(final String table,
                                        final String column,
                                        final Object value) {
        return sqlSelectAllByColumns(table, new String[]{column}, new Object[]{value});
    }

    /**
     * Generates SELECT sql query for a table.
     *
     * @param table   the database table
     * @param columns the column name
     * @param values  the column value
     * @return SQL query string
     */
    default String sqlSelectAllByColumns(final String table,
                                         final String[] columns,
                                         final Object[] values) {
        if (columns.length > 0 && columns.length == values.length) {
            final StringBuilder conditions = new StringBuilder();
            for (int i = 0; i < columns.length; i++) {
                if (conditions.length() > 0) {
                    conditions.append(" and ");
                }
                final String column = columns[i];
                final String value = getColumnStringValue(values[i]);
                conditions.append(column).append(" = ").append(value);
            }
            return sqlSelectAll(table) + " where " + conditions;
        }
        return "";
    }

    default String getColumnStringValue(Object value) {
        return value instanceof String || value instanceof LocalDateTime
                ? "'" + value + "'"
                : value.toString();
    }

    /**
     * Create a row in the table associated with the entity.
     *
     * @param entity the entity
     * @return {@link Optional} saved entity
     */
    default <X> Optional<X> save(final X entity,
                                 final Class<? extends X> clazz) {
        Field idColumn = getIdColumn(clazz);
        idColumn.setAccessible(true);
        try {
            idColumn.set(entity, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Optional<X> savedEntity = getDatabaseConnection().queryTransaction(sqlInsert(entity, clazz),
                resultSet -> Optional.ofNullable(getObject(resultSet, clazz)),
                Optional::empty);

        savedEntity.ifPresent(_entity -> {
            try {
                idColumn.set(entity, getIdColumnValue(clazz, _entity));
                populateOneToManyFields(entity, clazz);
                populateManyToManyFields(entity, clazz);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        return savedEntity;
    }

    /**
     * Fetch all entities from associated table,
     * where column has specified value.
     *
     * @param column table column
     * @param value  specified value
     * @return a list of found entities
     */
    default <X> List<? extends X> findByColumn(final String column,
                                               final Object value,
                                               final Class<? extends X> clazz) {
        String tableName = getTableName(clazz);
        Function<ResultSet, List<? extends X>> resultSetOperation = resultSet -> getObjects(resultSet, clazz);
        return findByColumn(tableName, column, value, resultSetOperation, Collections::emptyList);
    }

    /**
     * Fetch all entities from associated table,
     * where column has specified value.
     *
     * @param column             table column
     * @param value              specified value
     * @param tableName          table name
     * @param resultSetOperation result set operation
     * @param <X>                specific type of the
     * @return a list of found entities
     */
    default <X> X findByColumn(final String tableName,
                               final String column,
                               final Object value,
                               final Function<ResultSet, X> resultSetOperation,
                               final Supplier<X> defaultValue) {
        return findByColumns(
                tableName,
                new String[]{column},
                new Object[]{value},
                resultSetOperation,
                defaultValue);
    }

    /**
     * Fetch all entities from associated table,
     * where columns have specified value.
     *
     * @param columns            table columns
     * @param values             specified value
     * @param tableName          table name
     * @param resultSetOperation result set operation
     * @param <X>                specific type of the
     * @return a list of found entities
     */
    default <X> X findByColumns(final String tableName,
                                final String[] columns,
                                final Object[] values,
                                final Function<ResultSet, X> resultSetOperation,
                                final Supplier<X> defaultValue) {
        return Optional.ofNullable(columns)
                .map(_columns -> getDatabaseConnection().queryTransaction(
                        sqlSelectAllByColumns(tableName, _columns, values),
                        resultSetOperation,
                        defaultValue))
                .orElseGet(defaultValue);
    }

    /**
     * Find entity by id and given Class.
     *
     * @param id entity id
     * @return {@link Optional} found entity
     */
    default <X> Optional<X> findById(final Object id,
                                     final Class<? extends X> clazz) {
        return Optional.ofNullable(findByColumn(getIdColumnName(clazz), id, clazz).get(0));
    }

    /**
     * Extract entity from result set with given Class.
     *
     * @param resultSet the result set
     * @param clazz     entity class object
     * @param <X>       entity specific type
     * @return extracted entity
     */
    default <X> X getObject(final ResultSet resultSet,
                            final Class<? extends X> clazz) {
        final X entity = EMSUtils.getClassInstance(clazz);
        try {
            if (resultSet.next()) {
                populateColumnFields(resultSet, entity, clazz);
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
        return entity;
    }

    /**
     * Extract multiple entities from result set with a given entity Class.
     *
     * @param resultSet the result set
     * @param clazz     entity class object
     * @param <X>       entity specific type
     * @return a list of extracted entities
     */
    default <X> List<X> getObjects(final ResultSet resultSet,
                                   final Class<? extends X> clazz) {
        final List<X> list = new ArrayList<>();
        while (true) {
            Optional<? extends X> object = Optional.ofNullable(getObject(resultSet, clazz));
            object.ifPresent(list::add);
            if (!object.isPresent()) {
                break;
            }
        }
        return list;
    }

    /**
     * Populates entity fields from result set
     * using a map of column names and fields.
     *
     * @param resultSet  the result set
     * @param entity     the entity
     * @param columnsMap a map of column names and fields
     * @throws SQLException if a database access error occurs
     *                      or a method is called on a closed result set
     */
    default <X> void populateByColumnsMap(final ResultSet resultSet,
                                          final X entity,
                                          final Map<String, Field> columnsMap)
            throws SQLException {

        final ResultSetMetaData metaData = resultSet.getMetaData();
        final int columnCount = metaData.getColumnCount();
        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
            final String columnName = metaData.getColumnName(columnIndex);
            if (columnsMap.containsKey(columnName)) {
                final Field column = columnsMap.get(columnName);
                column.setAccessible(true);
                try {
                    Object object = resultSet.getObject(columnIndex, column.getType());
                    column.set(entity, object);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Populates all the entity fields associated with a column
     * in a table associated with the entity.
     *
     * @param resultSet the result set
     * @param entity    the entity
     * @throws SQLException if a database access error occurs
     *                      or a method is called on a closed result set
     */
    default <X> void populateColumnFields(final ResultSet resultSet,
                                          final X entity,
                                          final Class<? extends X> clazz)
            throws SQLException {
        populateByColumnsMap(resultSet, entity, getColumnsMap(clazz));
    }

    /**
     * Populate one {@link OneToMany} field.
     *
     * @param entity the entity
     * @param field  field that contains @OneToMany|@ManyToMany annotation
     *               and returns a table name
     * @throws IllegalAccessException when {@link Field#set(Object, Object)} method is called
     *                                and it is enforcing Java language access control and
     *                                the underlying field is either inaccessible or final
     */
    default <X> void populateOneToManyField(final X entity,
                                            final Field field,
                                            final Class<? extends X> clazz)
            throws IllegalAccessException {

        Object idColumnValue = getIdColumnValue(clazz, entity);

        if (Optional.ofNullable(idColumnValue).isPresent()) {

            final String query = sqlSelectAllByColumn(
                    EntityUtils.getTableName(getTargetEntity(field)),
                    getIdColumnName(clazz),
                    idColumnValue);

            Function<ResultSet, List<X>> resultSetListFunction = resultSet -> getObjects(resultSet, getTargetEntity(field));

            populateField(entity, field, query, resultSetListFunction, Collections::emptyList);
        }
    }

    /**
     * Populate {@link OneToMany} fields.
     *
     * @param entity the entity
     * @throws IllegalAccessException when {@link Field#set(Object, Object)} method is called
     *                                and it is enforcing Java language access control and
     *                                the underlying field is either inaccessible or final
     */
    default <X> void populateOneToManyFields(final X entity,
                                             final Class<? extends X> clazz)
            throws IllegalAccessException {
        for (Field field : getOneToManyFields(clazz)) {
            populateOneToManyField(entity, field, clazz);
        }
    }

    /**
     * Populate one {@link ManyToMany} field.
     *
     * @param entity the entity
     * @param field  field that contains @OneToMany|@ManyToMany annotation
     *               and returns a table name
     * @throws IllegalAccessException when {@link Field#set(Object, Object)} method is called
     *                                and it is enforcing Java language access control and
     *                                the underlying field is either inaccessible or final
     */
    default <X> void populateManyToManyField(final X entity,
                                             final Field field,
                                             final Class<? extends X> clazz)
            throws IllegalAccessException {

        if (Collection.class.isAssignableFrom(field.getType())) {

            final Object currentEntityIdColumnValue = getIdColumnValue(clazz, entity);

            if (Optional.ofNullable(currentEntityIdColumnValue).isPresent()) {
                final Class<? extends X> targetEntity = getTargetEntity(field);
                final String currentEntityIdColumnName = getIdColumnName(clazz);
                final String targetEntityIdColumnName = getIdColumnName(targetEntity);
                final String joinTableName = getJoinTableName(field);
                final String query = sqlSelectAllByColumn(
                        joinTableName,
                        currentEntityIdColumnName,
                        currentEntityIdColumnValue);

                field.setAccessible(true);
                Collection<?> objects = (Collection<?>) field.get(entity);

                Optional.ofNullable(objects)
                        .ifPresent(_objects -> _objects.forEach(_obj -> {
                            createRelationIfMissing(
                                    currentEntityIdColumnValue,
                                    targetEntity,
                                    currentEntityIdColumnName,
                                    targetEntityIdColumnName,
                                    joinTableName,
                                    _obj);
                        }));

                Function<ResultSet, List<X>> resultSetListFunction = resultSet -> fetchRelatedEntities(resultSet,
                        currentEntityIdColumnName,
                        targetEntityIdColumnName,
                        targetEntity);

                populateField(entity, field, query, resultSetListFunction, Collections::emptyList);
            }
        } else {
            throw new InvalidMappingException("@ManyToMany annotated field is not of a collection type!");
        }
    }

    default <X> void createRelationIfMissing(Object currentEntityIdColumnValue,
                                             Class<? extends X> targetEntity,
                                             String currentEntityIdColumnName,
                                             String targetEntityIdColumnName,
                                             String joinTableName,
                                             Object _obj) {
        Object targetEntityIdColumnValue = getIdColumnValue(targetEntity, _obj);
        String[] idColumnNames = {currentEntityIdColumnName, targetEntityIdColumnName};
        Object[] idColumnValues = {currentEntityIdColumnValue, targetEntityIdColumnValue};
        Function<ResultSet, Boolean> relationExists = resultSet -> relationExists(
                currentEntityIdColumnValue,
                currentEntityIdColumnName,
                targetEntityIdColumnName,
                targetEntityIdColumnValue,
                resultSet);
        Boolean entryExists = findByColumns(
                joinTableName,
                idColumnNames,
                idColumnValues,
                relationExists,
                () -> false);
        if (!entryExists) {
            final String _query = sqlInsert(
                    joinTableName,
                    idColumnNames,
                    idColumnValues);
            getDatabaseConnection().queryTransaction(_query, relationExists, () -> false);
        }
    }

    default Boolean relationExists(final Object currentEntityIdColumnValue,
                                   final String currentEntityIdColumnName,
                                   final String targetEntityIdColumnName,
                                   final Object targetEntityIdColumnValue,
                                   final ResultSet resultSet) {
        try {
            while (resultSet.next()) {
                Optional<Object> c = Optional.ofNullable(resultSet.getObject(currentEntityIdColumnName));
                Optional<Object> t = Optional.ofNullable(resultSet.getObject(targetEntityIdColumnName));
                if (c.isPresent() && t.isPresent()
                        && c.get().equals(currentEntityIdColumnValue)
                        && t.get().equals(targetEntityIdColumnValue)) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    default <X, Y> void populateField(final X entity,
                                      final Field field,
                                      final String query,
                                      final Function<ResultSet, Y> resultSetFunction,
                                      final Supplier<Y> defaultValue) throws IllegalAccessException {
        final Y result = getDatabaseConnection().queryTransaction(query, resultSetFunction, defaultValue);
        field.setAccessible(true);
        field.set(entity, result);
    }

    /**
     * Fetch all the entities related to the current entity.
     *
     * @param resultSet                 the result set
     * @param currentEntityIdColumnName current entity id column name
     * @param targetEntityIdColumnName  target entity id column name
     * @param targetEntity              class object of the target entity
     * @param <X>                       specific type of the target entity
     * @return a list of related entities
     */
    default <X> List<X> fetchRelatedEntities(final ResultSet resultSet,
                                             final String currentEntityIdColumnName,
                                             final String targetEntityIdColumnName,
                                             final Class<? extends X> targetEntity) {
        final List<X> list = new ArrayList<>();
        try {
            final ResultSetMetaData metaData = resultSet.getMetaData();
            if (validManyToManyResultSet(metaData, currentEntityIdColumnName, targetEntityIdColumnName)) {
                while (resultSet.next()) {
                    Object targetEntityIdColumnValue = targetEntityIdColumnName.equals(metaData.getColumnName(1))
                            ? resultSet.getObject(1)
                            : resultSet.getObject(2);
                    findById(targetEntityIdColumnValue, targetEntity)
                            .ifPresent(list::add);
                }
            } else {
                throw new InvalidResultSetException("Result set columns do not match related entities' id columns!");
            }
        } catch (SQLException | InvalidResultSetException e) {
            e.printStackTrace();
            return emptyList();
        }
        return list;
    }

    /**
     * Validate {@link ManyToMany} join-table.
     *
     * @param metaData                  result set meta-data
     * @param currentEntityIdColumnName current entity id column name
     * @param targetEntityIdColumnName  target entity id column name
     * @return TRUE if result set consists of 2 columns and
     * each column has an association with the entity
     * @throws SQLException if a database access error occurs
     *                      or a method is called on a closed result set
     */
    default boolean validManyToManyResultSet(final ResultSetMetaData metaData,
                                             final String currentEntityIdColumnName,
                                             final String targetEntityIdColumnName) throws SQLException {
        return Objects.nonNull(metaData) &&
                metaData.getColumnCount() == 2 &&
                (metaData.getColumnName(1).equals(currentEntityIdColumnName) ||
                        metaData.getColumnName(2).equals(currentEntityIdColumnName)) &&
                (metaData.getColumnName(1).equals(targetEntityIdColumnName) ||
                        metaData.getColumnName(2).equals(targetEntityIdColumnName));
    }

    /**
     * Populate {@link ManyToMany} fields.
     *
     * @param entity the entity
     * @throws IllegalAccessException when {@link Field#set(Object, Object)} method is called
     *                                and it is enforcing Java language access control and
     *                                the underlying field is either inaccessible or final
     */
    default <X> void populateManyToManyFields(final X entity,
                                              final Class<? extends X> clazz) throws IllegalAccessException {
        for (Field field : getManyToManyFields(clazz)) {
            populateManyToManyField(entity, field, clazz);
        }
    }
}
