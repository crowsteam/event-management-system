package com.endava.internship.ems.dao.repositories;

import com.endava.internship.ems.dao.config.DatabaseConnection;
import com.endava.internship.ems.services.model.entities.User;
import com.endava.internship.ems.dao.repositories.interfaces.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserRepository implements IUserRepository {

    private final DatabaseConnection dbConnection;

    @Autowired
    public UserRepository(DatabaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    @SuppressWarnings("ReturnPrivateMutableField")
    public DatabaseConnection getDatabaseConnection() {
        return dbConnection;
    }

    public List<User> findByName(String name) {
        return findByColumn("user_name", name);
    }
}
