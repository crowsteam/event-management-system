package com.endava.internship.ems.dao.repositories.interfaces;

import com.endava.internship.ems.services.model.entities.Event;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface IEventRepository extends GenericRepository<Event> {

    List<Event> findByName(String name);
}
