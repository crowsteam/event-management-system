package com.endava.internship.ems.dao.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Function;
import java.util.function.Supplier;

@Component
public class DatabaseConnection {

    private final DataSource dataSource;

    @Autowired
    public DatabaseConnection(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> T queryTransaction(final String query,
                                  final Function<ResultSet, T> operation,
                                  final Supplier<T> defaultResult) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(query)) {
                connection.commit();
                return operation.apply(resultSet);
            } catch (Exception e) {
                connection.rollback();
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return defaultResult.get();
    }
}
