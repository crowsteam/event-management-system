package com.endava.internship.ems.dao.repositories.interfaces;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.endava.internship.ems.dao.utils.EntityUtils.*;
import static java.util.Collections.emptyList;

public interface GenericRepository<T> extends AbstractRepository {

    /**
     * Get the entity class.
     *
     * @return the class of the entity
     */
    Class<T> getEntityClass();

    /**
     * Get database table name associated with the entity.
     *
     * @return table name
     */
    default String getEntityTableName() {
        return getTableName(getEntityClass());
    }

    /**
     * Create a row in the table associated with the entity.
     *
     * @param entity the entity
     * @return {@link Optional} saved entity
     */
    default Optional<T> save(final T entity) {
        return save(entity, getEntityClass());
    }

    /**
     * Update a row in the table associated with the entity.
     *
     * @param entity the entity
     * @return {@link Optional} updated entity
     */
    default Optional<T> update(final T entity) {
        return findById(getIdColumnValue(getEntityClass(), entity))
                .flatMap(_entity -> getDatabaseConnection()
                        .queryTransaction(sqlUpdate(_entity, getEntityClass()),
                                resultSet -> Optional.ofNullable(getObject(resultSet)),
                                Optional::empty));
    }

    /**
     * Remove a row in the table associated with the entity.
     *
     * @param entity the entity
     * @return {@link Optional} removed entity
     */
    default Optional<T> remove(final T entity) {
        return findById(getIdColumnValue(getEntityClass(), entity))
                .flatMap(_entity -> getDatabaseConnection()
                        .queryTransaction(sqlRemove(_entity, getEntityClass()),
                                resultSet -> Optional.ofNullable(getObject(resultSet)),
                                Optional::empty));
    }

    /**
     * Find all entities, having entity fields equal to related columns.
     *
     * @param entity the entity
     * @return a list of found entities
     */
    default List<T> find(final T entity) {
        return Optional.ofNullable(entity)
                .map(_entity -> getDatabaseConnection().queryTransaction(
                        sqlSelectAllByColumns(
                                getEntityTableName(),
                                getColumnNames(getEntityClass()),
                                getColumnValues(getEntityClass(), entity)),
                        this::getObjects,
                        ArrayList<T>::new))
                .orElse(emptyList());
    }

    /**
     * Find entity by id.
     *
     * @param id entity id
     * @return {@link Optional} found entity
     */
    default Optional<T> findById(final Object id) {
        return Optional.ofNullable(findByColumn(getIdColumnName(getEntityClass()), id).get(0));
    }

    /**
     * Fetch all entities.
     *
     * @return a list of found entities
     */
    default List<T> findAll() {
        return getDatabaseConnection()
                .queryTransaction(sqlSelectAll(getEntityTableName()), this::getObjects, Collections::emptyList);
    }

    /**
     * Fetch all entities where from associated table,
     * where column has specified value.
     *
     * @param column table column
     * @param value  specified value
     * @return a list of found entities
     */
    default List<T> findByColumn(final String column,
                                 final Object value) {
        return Optional.ofNullable(column)
                .map(_field -> getDatabaseConnection().queryTransaction(
                        sqlSelectAllByColumn(getEntityTableName(), _field, value),
                        this::getObjects,
                        ArrayList<T>::new))
                .orElse(emptyList());
    }

    /**
     * Extract entity from result set
     *
     * @param resultSet the result set
     * @return extracted entity
     */
    default T getObject(final ResultSet resultSet) {
        return getObject(resultSet, getEntityClass());
    }

    /**
     * Extract multiple entities from result set.
     *
     * @param resultSet the result set
     * @return a list of extracted entities
     */
    default List<T> getObjects(final ResultSet resultSet) {
        return getObjects(resultSet, getEntityClass());
    }
}
