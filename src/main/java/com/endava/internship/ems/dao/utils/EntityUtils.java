package com.endava.internship.ems.dao.utils;

import com.endava.internship.ems.services.model.entities.annotations.*;
import com.endava.internship.ems.services.exceptions.InvalidMappingException;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.endava.internship.ems.dao.utils.EMSUtils.concatenateArrays;
import static com.endava.internship.ems.dao.utils.EMSUtils.toSnakeCase;
import static java.util.stream.Collectors.toMap;

public class EntityUtils {

    public static Field[] getFieldsAnnotatedWith(final Class<? extends Annotation> annotation,
                                                 final Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(annotation))
                .toArray(Field[]::new);
    }

    public static Map<String, Field> getFieldsMap(final Field[] fields,
                                                  final Function<Field, String> keyGenerator) {
        return Arrays.stream(fields).collect(toMap(keyGenerator, field -> field));
    }

    public static String getTableName(final Class<?> clazz) {
        if (Optional.ofNullable(clazz).isPresent() &&
                !clazz.isAnnotationPresent(Table.class)) {
            throw new InvalidMappingException(
                    "Missing @Table annotation in class " + clazz.getName());
        }
        return Optional.ofNullable(clazz)
                .map(_clazz -> _clazz.getAnnotation(Table.class))
                .map(Table::value)
                .orElse("");
    }

    public static String getTableName(final Object entity) {
        return Optional.ofNullable(entity)
                .map(EntityUtils::getTableName)
                .orElse("");
    }

    public static Field getIdColumn(final Class<?> clazz) {
        final List<Field> idFields = Arrays.asList(getFieldsAnnotatedWith(Id.class, clazz));
        if (idFields.isEmpty()) {
            throw new InvalidMappingException(
                    "Missing @Id annotation in class " + clazz.getName());
        }
        if (idFields.size() > 1) {
            throw new InvalidMappingException(
                    "Too many @Id annotated columns in class " + clazz.getName());
        }
        return idFields.get(0);
    }

    public static String getIdColumnName(final Class<?> clazz) {
        return getIdColumn(clazz).getAnnotation(Column.class).value();
    }

    public static Field[] getColumns(final Class<?> clazz) {
        return concatenateArrays(
                Field.class,
                getColumnFields(clazz),
                getOneToOneFields(clazz),
                getManyToOneFields(clazz));
    }

    public static Field[] getColumnFields(final Class<?> clazz) {
        return getFieldsAnnotatedWith(Column.class, clazz);
    }

    public static String getColumnName(final Field field) {
        return Optional.ofNullable(field)
                .filter(f -> f.isAnnotationPresent(Column.class))
                .map(f -> f.getAnnotation(Column.class).value())
                .orElse(getJoinColumnName(field));
    }

    public static String[] getColumnNames(final Class<?> clazz) {
        return Arrays.stream(getColumns(clazz))
                .map(EntityUtils::getColumnName)
                .toArray(String[]::new);
    }

    public static String getJoinColumnName(final Field field) {
        return Optional.ofNullable(field)
                .map(_field -> Optional.of(_field)
                        .filter(f -> f.isAnnotationPresent(JoinColumn.class))
                        .map(f -> f.getAnnotation(JoinColumn.class).value())
                        .orElse(toSnakeCase(_field.getName(), String::toLowerCase)))
                .orElse("");
    }

    public static Map<String, Field> getColumnsMap(final Class<?> clazz) {
        return getFieldsMap(getColumns(clazz), EntityUtils::getColumnName);
    }

    public static Field[] getOneToOneFields(final Class<?> clazz) {
        return getFieldsAnnotatedWith(OneToOne.class, clazz);
    }

    public static Field[] getManyToOneFields(final Class<?> clazz) {
        return getFieldsAnnotatedWith(ManyToOne.class, clazz);
    }

    public static Field[] getOneToManyFields(final Class<?> clazz) {
        return getFieldsAnnotatedWith(OneToMany.class, clazz);
    }

    public static <X> Class<X> getTargetEntity(final Field field) {
        final Optional<Class<X>> oneToOneTargetEntity =
                Optional.ofNullable(getOneToOneTargetEntity(field));
        final Optional<Class<X>> manyToOneTargetEntity =
                Optional.ofNullable(getManyToOneTargetEntity(field));
        final Optional<Class<X>> oneToManyTargetEntity =
                Optional.ofNullable(getOneToManyTargetEntity(field));
        final Optional<Class<X>> manyToManyTargetEntity =
                Optional.ofNullable(getManyToManyTargetEntity(field));

        return oneToOneTargetEntity
                .map(Optional::of)
                .orElse(manyToOneTargetEntity)
                .map(Optional::of)
                .orElse(oneToManyTargetEntity)
                .map(Optional::of)
                .orElse(manyToManyTargetEntity)
                .orElse(null);

    }

    @SuppressWarnings("unchecked")
    public static <X> Class<X> getOneToOneTargetEntity(final Field field) {
        return (Class<X>) Optional.ofNullable(field)
                .filter(f -> f.isAnnotationPresent(OneToOne.class))
                .map(f -> f.getAnnotation(OneToOne.class))
                .map(OneToOne::targetEntity)
                .orElse(null);
    }

    @SuppressWarnings("unchecked")
    public static <X> Class<X> getManyToOneTargetEntity(final Field field) {
        return (Class<X>) Optional.ofNullable(field)
                .filter(f -> f.isAnnotationPresent(ManyToOne.class))
                .map(f -> f.getAnnotation(ManyToOne.class))
                .map(ManyToOne::targetEntity)
                .orElse(null);
    }

    @SuppressWarnings("unchecked")
    public static <X> Class<X> getOneToManyTargetEntity(final Field field) {
        return (Class<X>) Optional.ofNullable(field)
                .filter(f -> f.isAnnotationPresent(OneToMany.class))
                .map(f -> f.getAnnotation(OneToMany.class))
                .map(OneToMany::targetEntity)
                .orElse(null);
    }

    @SuppressWarnings("unchecked")
    public static <X> Class<X> getManyToManyTargetEntity(final Field field) {
        return (Class<X>) Optional.ofNullable(field)
                .filter(f -> f.isAnnotationPresent(ManyToMany.class))
                .map(f -> f.getAnnotation(ManyToMany.class))
                .map(ManyToMany::targetEntity)
                .orElse(null);
    }

    public static String getJoinTableName(final Field field) {
        return field.getAnnotation(JoinTable.class).value();
    }

    public static Field[] getManyToManyFields(final Class<?> clazz) {
        final Field[] fields = getFieldsAnnotatedWith(ManyToMany.class, clazz);
        for (Field field : fields) {
            if (!field.isAnnotationPresent(JoinTable.class)) {
                throw new InvalidMappingException(
                        "Missing @JoinTable annotation in class" + clazz +
                                " on field " + field.getName());
            }
        }
        return fields;
    }

    public static Field[] getToManyFields(final Class<?> clazz) {
        return concatenateArrays(
                Field.class,
                getOneToManyFields(clazz),
                getManyToManyFields(clazz));
    }

    @JsonIgnore
    public static Object getColumnValue(final Object entity, final Field field) {
        return Optional.ofNullable(field)
                .filter(_field -> _field.isAnnotationPresent(Column.class))
                .map(column -> {
                    try {
                        column.setAccessible(true);
                        return column.get(entity);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        return null;
                    }
                }).orElse(null);
    }

    @JsonIgnore
    public static Object[] getColumnValues(final Class<?> clazz, final Object entity) {
        return Arrays.stream(getColumns(clazz))
                .map(column -> getColumnValue(entity, column))
                .toArray();
    }

    @JsonIgnore
    public static Object getIdColumnValue(final Class<?> clazz, final Object entity) {
        return getColumnValue(entity, getIdColumn(clazz));
    }
}
