package com.endava.internship.ems.dao.repositories.interfaces;

import com.endava.internship.ems.services.model.entities.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface IUserRepository extends GenericRepository<User> {

    List<User> findByName(String name);
}
