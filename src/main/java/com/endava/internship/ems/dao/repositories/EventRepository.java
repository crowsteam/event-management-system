package com.endava.internship.ems.dao.repositories;

import com.endava.internship.ems.dao.config.DatabaseConnection;
import com.endava.internship.ems.services.model.entities.Event;
import com.endava.internship.ems.dao.repositories.interfaces.IEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EventRepository implements IEventRepository {

    private final DatabaseConnection dbConnection;

    @Autowired
    public EventRepository(DatabaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public Class<Event> getEntityClass() {
        return Event.class;
    }

    @Override
    @SuppressWarnings("ReturnPrivateMutableField")
    public DatabaseConnection getDatabaseConnection() {
        return dbConnection;
    }

    @Override
    public List<Event> findByName(String name) {
        return findByColumn("event_name", name);
    }
}
