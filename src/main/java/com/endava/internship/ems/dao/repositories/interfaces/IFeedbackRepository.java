package com.endava.internship.ems.dao.repositories.interfaces;

import com.endava.internship.ems.services.model.entities.Feedback;

public interface IFeedbackRepository extends GenericRepository<Feedback> {
}
