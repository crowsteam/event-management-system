# Event Management System

### About the project

###### Project requirements

* create company's events
* display event's info, share it (eg. facebook)
* write feedback on event

###### Project features

* log in/log out
* create event
* delete event
* search event by filter
* add feedback
* delete feedback
* invite user to an event

###### Project structure (technical stuff here)

* create database DDL scripts (Flyway)
* ...

###### Team

* Alexandru Rusnac (Team Lead)
* Elena Grosu (Senorita Developer)
* Alexandru Dimov (Hacker)
* Stanislav Girlea (Consultant)
